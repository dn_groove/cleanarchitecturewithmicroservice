﻿using Campaign.Domain.Enums;
using MediatR;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Campaign.Application.Commands
{
    public class CreateApplicationCommand : IRequest<bool>
    {
        public string Name { get; set; }
        public DeviceType DeviceType { get; set; }
    }
}
