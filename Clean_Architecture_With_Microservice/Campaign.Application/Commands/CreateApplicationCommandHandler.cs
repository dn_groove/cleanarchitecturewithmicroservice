﻿using Campaign.Application.Ports.Repository;
using Campaign.Domain.Appgregates;
using MediatR;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;

namespace Campaign.Application.Commands
{
    public class CreateApplicationCommandHandler : IRequestHandler<CreateApplicationCommand, bool>
    {
        private readonly IApplicationRepository _applicationRepository;
        public CreateApplicationCommandHandler(IApplicationRepository applicationRepository)
        {
            _applicationRepository = applicationRepository;
        }

        public async Task<bool> Handle(CreateApplicationCommand request, CancellationToken cancellationToken)
        {

            var application = new ApplicationAggregate(request.Name, Guid.NewGuid().ToString(), request.DeviceType);
            await _applicationRepository.CreateAsync(application);
            return await Task.FromResult(true);
        }
    }
}
