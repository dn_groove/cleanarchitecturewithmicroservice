﻿using Campaign.Application.Ports.Repository;
using Campaign.Domain.Appgregates;
using MediatR;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;

namespace Campaign.Application.Commands
{
    public class CreateCampaignCommandHandler : IRequestHandler<CreateCampaignCommand, bool>
    {
        private readonly ICampaignRepository _campaignRepository;
        public CreateCampaignCommandHandler(ICampaignRepository campaignRepository)
        {
            _campaignRepository = campaignRepository;
        }

        public async Task<bool> Handle(CreateCampaignCommand request, CancellationToken cancellationToken)
        {
            var campaign = new CampaignAggregate(request.Name, request.Budget, 0, request.DeviceType);
            await _campaignRepository.CreateAsync(campaign);
            return await Task.FromResult(true);
        }
    }
}
