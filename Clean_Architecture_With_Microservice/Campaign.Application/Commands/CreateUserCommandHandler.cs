﻿using Campaign.Application.Ports.Repository;
using Campaign.Domain.Appgregates;
using MediatR;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;

namespace Campaign.Application.Commands
{
    public class CreateUserCommandHandler : IRequestHandler<CreateUserCommand, bool>
    {
        private readonly IUserRepository _userRepository;
        public CreateUserCommandHandler(IUserRepository userRepository)
        {
            _userRepository = userRepository;
        }

        async Task<bool> IRequestHandler<CreateUserCommand, bool>.Handle(CreateUserCommand request, CancellationToken cancellationToken)
        {

            var entity = new UserAggregate(request.Name, request.Password, request.Email);
            await _userRepository.CreateAsync(entity);
            return await Task.FromResult(true);
        }
    }
}
