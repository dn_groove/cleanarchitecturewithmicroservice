﻿using Campaign.Application.Queries;
using Campaign.Application.Queries.Interfaces;
using Microsoft.Extensions.DependencyInjection;
using System;
using System.Collections.Generic;
using System.Text;

namespace Campaign.Application.Configs
{
    public static class ApplicationConfigs
    {
        public static IServiceCollection AddQueryServiceConfigs(this IServiceCollection services)
        {
            services.AddScoped<IApplicattionQueryService, ApplicationQueryService>();
            services.AddScoped<ICampaignQueryService, CampaignQueryService>();
            services.AddScoped<IUserQueryService, UserQueryService>();
            return services;
        }

        public static IServiceCollection AddCommandValidationConfigs(this IServiceCollection services)
        {
            return services;
        }
    }
}
