﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Threading.Tasks;

namespace Campaign.Application.Ports.ConnectionFactory
{
    public interface IConnectionFactory
    {
        IDbConnection GetConnection();
    }
}
