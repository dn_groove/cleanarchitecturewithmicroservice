﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Threading.Tasks;

namespace Campaign.Application.Ports.QueryService
{
    public interface IQueryService
    {
        IEnumerable<T> QueryDataSet<T>(string sql, object param, CommandType commandType);
        Task<IEnumerable<T>> QueryDataSetAsync<T>(string sql, object param, CommandType commandType);
        T QueryFirst<T>(string sql, object param, CommandType commandType);
        Task<T> QueryFirstAsync<T>(string sql, object param, CommandType commandType);
        T QueryFirstOrDefault<T>(string sql, object param, CommandType commandType);
        Task<T> QueryFirstOrDefaultAsync<T>(string sql, object param, CommandType commandType);
        T QuerySingle<T>(string sql, object param, CommandType commandType);
        Task<T> QuerySingleAsync<T>(string sql, object param, CommandType commandType);
        T QuerySingleOrDefault<T>(string sql, object param, CommandType commandType);
        Task<T> QuerySingleOrDefaultAsync<T>(string sql, object param, CommandType commandType);
        Task ExecuteAsync(string sql, object param, CommandType commandType);

        Task<IEnumerable<TParent>> QueryParentChildAsync<TParent, TChild, TParentKey>(string sql, Dictionary<TParentKey, TParent> cache,
            Func<TParent, TChild, TParent> map, dynamic param = null, string splitOn = "Id", IDbTransaction transaction = null,
            bool buffered = true, int? commandTimeout = null, CommandType? commandType = null);
    }
}
