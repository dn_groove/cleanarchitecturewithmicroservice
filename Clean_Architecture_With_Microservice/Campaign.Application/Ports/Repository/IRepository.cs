﻿using Campaign.Domain.SharedKernel;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Campaign.Application.Ports.Repository
{
    public interface IRepository<TEntity> where TEntity : Entity
    {
        Task CreateAsync(TEntity entity);
        TEntity GetById(int id);
        IEnumerable<TEntity> List();
        void Delete(TEntity entity);
        void Update(TEntity entity);
        Task<TEntity> GetByIdAsync(int id);
    }
}
