﻿using Campaign.Application.Ports.QueryService;
using Campaign.Application.Queries.Dto;
using Campaign.Application.Queries.Interfaces;
using System;
using System.Collections.Generic;
using System.Data;
using System.Text;
using System.Threading.Tasks;

namespace Campaign.Application.Queries
{
    public class ApplicationQueryService : IApplicattionQueryService
    {
        private readonly IQueryService _queryService;
        public ApplicationQueryService(IQueryService queryService)
        {
            _queryService = queryService;
        }

        public async Task<IEnumerable<ApplicationListDto>> GetListAsync()
        {
            var query = "SELECT * FROM Applications";
            var result = await _queryService.QueryDataSetAsync<ApplicationListDto>(query, null, CommandType.Text);
            return result;
        }
    }
}
