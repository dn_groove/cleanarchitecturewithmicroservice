﻿using Campaign.Application.Ports.QueryService;
using Campaign.Application.Queries.Dto;
using Campaign.Application.Queries.Interfaces;
using System;
using System.Collections.Generic;
using System.Data;
using System.Text;
using System.Threading.Tasks;

namespace Campaign.Application.Queries
{
    public class CampaignQueryService : ICampaignQueryService
    {
        private readonly IQueryService _queryService;
        public CampaignQueryService(IQueryService queryService)
        {
            _queryService = queryService;
        }

        public async Task<IEnumerable<CampaignListDto>> GetListAsync()
        {
            var query = "SELECT * FROM Campaigns";
            var result = await _queryService.QueryDataSetAsync<CampaignListDto>(query, null, CommandType.Text);
            return result;
        }
    }
}
