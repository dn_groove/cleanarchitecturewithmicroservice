﻿using Campaign.Domain.Enums;
using System;
using System.Collections.Generic;
using System.Text;

namespace Campaign.Application.Queries.Dto
{
    public class ApplicationListDto
    {
        public int Id { get; set; }
        public string Name { get; set; }
        public string AppKey { get; set; }
        public DeviceType DeviceType { get; set; }
    }
}
