﻿using Campaign.Domain.Enums;
using System;
using System.Collections.Generic;
using System.Text;

namespace Campaign.Application.Queries.Dto
{
    public class CampaignListDto
    {
        public int Id { get; set; }
        public string Name { get; set; }
        public int Budget { get; set; }
        public int UsedBudget { get; set; }
        public DeviceType DeviceType { get; set; }
    }
}
