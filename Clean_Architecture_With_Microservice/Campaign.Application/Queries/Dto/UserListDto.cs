﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Campaign.Application.Queries.Dto
{
    public class UserListDto
    {
        public Guid Id { get; set; }
        public string Name { get; set; }
        public string PasswordHash { get; set; }
        public string Email { get; set; }
    }
}
