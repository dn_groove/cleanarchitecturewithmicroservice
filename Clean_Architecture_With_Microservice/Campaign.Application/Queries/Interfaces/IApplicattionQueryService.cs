﻿using Campaign.Application.Queries.Dto;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;

namespace Campaign.Application.Queries.Interfaces
{
    public interface IApplicattionQueryService
    {
        Task<IEnumerable<ApplicationListDto>> GetListAsync();
    }
}
