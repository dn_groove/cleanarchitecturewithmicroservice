﻿using Campaign.Application.Ports.QueryService;
using Campaign.Application.Queries.Dto;
using Campaign.Application.Queries.Interfaces;
using System;
using System.Collections.Generic;
using System.Data;
using System.Text;
using System.Threading.Tasks;

namespace Campaign.Application.Queries
{
    public class UserQueryService : IUserQueryService
    {
        private readonly IQueryService _queryService;
        public UserQueryService(IQueryService queryService)
        {
            _queryService = queryService;
        }

        public async Task<IEnumerable<UserListDto>> GetListAsync()
        {
            var query = "SELECT * FROM Users";
            var result = await _queryService.QueryDataSetAsync<UserListDto>(query, null, CommandType.Text);
            return result;
        }
    }
}
