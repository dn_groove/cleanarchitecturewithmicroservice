﻿using Campaign.Application.Ports.ConnectionFactory;
using Microsoft.Extensions.DependencyInjection;
using System;
using System.Collections.Generic;
using System.Text;

namespace Campaign.ConnectionFactory.Configs
{
    public static class ConnectionFactoryConfigs
    {
        public static IServiceCollection AddConnectionFactoryConfigs(this IServiceCollection services)
        {
            services.AddScoped<IConnectionFactory, ConnectionFactory>();
            return services;
        }
    }
}
