﻿using Campaign.Application.Ports.Repository;
using Campaign.Repository.Repositories;
using Microsoft.Extensions.DependencyInjection;
using System;
using System.Collections.Generic;
using System.Text;

namespace Campaign.Repository.Configs
{
    public static class RepositoryConfigs
    {
        public static IServiceCollection AddRepositoryConfigs(this IServiceCollection services)
        {
            services.AddScoped<ICampaignRepository, CampaignRepository>();
            services.AddScoped<IApplicationRepository, ApplicationRepository>();
            services.AddScoped<IUserRepository, UserRepository>();
            return services;
        }
    }
}
