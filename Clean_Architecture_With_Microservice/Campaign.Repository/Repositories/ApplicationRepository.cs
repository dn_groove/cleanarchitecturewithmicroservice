﻿using Campaign.Application.Ports.Repository;
using Campaign.Domain.Appgregates;
using Campaign.Domain.SharedKernel;
using Campaign.EntityFramework;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;

namespace Campaign.Repository.Repositories
{
    public class ApplicationRepository : Repository<ApplicationAggregate>, IApplicationRepository
    {
        public ApplicationRepository(ApplicationDBContext dbContext) : base(dbContext)
        {
        }
    }
}
