﻿using Campaign.Application.Ports.Repository;
using Campaign.Domain.Appgregates;
using Campaign.EntityFramework;
using System;
using System.Collections.Generic;
using System.Text;

namespace Campaign.Repository.Repositories
{
    public class CampaignRepository : Repository<CampaignAggregate>, ICampaignRepository
    {
        public CampaignRepository(ApplicationDBContext dbContext) : base(dbContext)
        {
        }
    }
}
