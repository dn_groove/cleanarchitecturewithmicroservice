﻿using Campaign.Application.Ports.Repository;
using Campaign.Domain.Appgregates;
using Campaign.EntityFramework;
using System;
using System.Collections.Generic;
using System.Text;

namespace Campaign.Repository.Repositories
{
    class UserRepository : Repository<UserAggregate>, IUserRepository
    {
        public UserRepository(ApplicationDBContext dbContext) : base(dbContext)
        {
        }
    }
}
