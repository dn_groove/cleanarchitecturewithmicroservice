﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Campaign.Application.Commands;
using Campaign.Application.Queries.Interfaces;
using MediatR;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;

namespace Campaign.Api.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class ApplicationController : ControllerBase
    {
        private readonly IMediator _mediator;
        private readonly IApplicattionQueryService _applicationQueryService;
        public ApplicationController(IMediator mediator, IApplicattionQueryService applicationQueryService)
        {
            _mediator = mediator;
            _applicationQueryService = applicationQueryService;
        }

        // GET api/values
        [HttpGet]
        public async Task<ActionResult<IEnumerable<string>>> GetAsync()
        {
            var result = await _applicationQueryService.GetListAsync();
            return new OkObjectResult(result);
        }

        // POST api/values
        [HttpPost]
        public async Task PostAsync([FromBody] CreateApplicationCommand command)
        {
            await _mediator.Send(command);
        }
    }
}