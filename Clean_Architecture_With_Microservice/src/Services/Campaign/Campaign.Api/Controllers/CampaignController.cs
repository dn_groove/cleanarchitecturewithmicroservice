﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Campaign.Application.Commands;
using Campaign.Application.Queries.Interfaces;
using MediatR;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;

namespace Campaign.Api.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class CampaignController : ControllerBase
    {
        private readonly IMediator _mediator;
        private readonly ICampaignQueryService _campaignQueryService;
        public CampaignController(IMediator mediator, ICampaignQueryService campaignQueryService )
        {
            _mediator = mediator;
            _campaignQueryService = campaignQueryService;
        }

        // GET api/values
        [HttpGet]
        public async Task<ActionResult<IEnumerable<string>>> GetAsync()
        {
            var result = await _campaignQueryService.GetListAsync();
            return new OkObjectResult(result);
        }

        // POST api/values
        [HttpPost]
        public async Task PostAsync([FromBody] CreateCampaignCommand command)
        {
            await _mediator.Send(command);
        }
    }
}