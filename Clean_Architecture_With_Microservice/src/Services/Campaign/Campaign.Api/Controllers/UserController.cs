﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Campaign.Application.Commands;
using Campaign.Application.Queries.Interfaces;
using MediatR;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;

namespace Campaign.Api.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class UserController : ControllerBase
    {
        private readonly IMediator _mediator;
        private readonly IUserQueryService _userQueryService;
        public UserController(IMediator mediator, IUserQueryService userQueryService)
        {
            _mediator = mediator;
            _userQueryService = userQueryService;
        }

        // GET api/values
        [HttpGet]
        public async Task<ActionResult<IEnumerable<string>>> GetAsync()
        {
            var result = await _userQueryService.GetListAsync();
            return new OkObjectResult(result);
        }

        // POST api/values
        [HttpPost]
        public async Task PostAsync([FromBody] CreateUserCommand command)
        {
            await _mediator.Send(command);
        }
    }
}