﻿using Campaign.Application.Commands;
using Campaign.Application.Configs;
using Campaign.ConnectionFactory.Configs;
using Campaign.Dapper.Configs;
using Campaign.EntityFramework.Configs;
using Campaign.Repository.Configs;
using MediatR;
using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;

namespace Campaign.Api
{
    public class Startup
    {
        public Startup(IConfiguration configuration)
        {
            Configuration = configuration;
        }

        public IConfiguration Configuration { get; }

        // This method gets called by the runtime. Use this method to add services to the container.
        public void ConfigureServices(IServiceCollection services)
        {
            services.AddMvc().SetCompatibilityVersion(CompatibilityVersion.Version_2_2);

            // Application Core Configs
            services.AddMediatR(typeof(CreateUserCommandHandler).Assembly);
            services.AddMediatR(typeof(CreateCampaignCommandHandler).Assembly);
            services.AddMediatR(typeof(CreateApplicationCommandHandler).Assembly);
            services.AddQueryServiceConfigs();
            // Infrastructure Configs
            services.AddEntityFrameworkConfigs(Configuration);
            services.AddRepositoryConfigs();
            services.AddDapperConfigs();
            services.AddConnectionFactoryConfigs();
        }

        // This method gets called by the runtime. Use this method to configure the HTTP request pipeline.
        public void Configure(IApplicationBuilder app, IHostingEnvironment env)
        {
            if (env.IsDevelopment())
            {
                app.UseDeveloperExceptionPage();
            }
            else
            {
                // The default HSTS value is 30 days. You may want to change this for production scenarios, see https://aka.ms/aspnetcore-hsts.
                app.UseHsts();
            }

            app.UseHttpsRedirection();
            app.UseMvc();
        }
    }
}
