﻿using Campaign.Application.Ports.QueryService;
using Microsoft.Extensions.DependencyInjection;
using System;
using System.Collections.Generic;
using System.Text;

namespace Campaign.Dapper.Configs
{
    public static class DapperConfigs
    {
        public static IServiceCollection AddDapperConfigs(this IServiceCollection services)
        {
            services.AddScoped<IQueryService, DapperQuery>();
            return services;
        }
    }
}
