﻿using Campaign.Application.Ports.ConnectionFactory;
using Campaign.Application.Ports.QueryService;
using Dapper;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Campaign.Dapper
{
    public class DapperQuery : IQueryService
    {
        private readonly IConnectionFactory _connectionFactory;
        public DapperQuery(IConnectionFactory connectionFactory)
        {
            _connectionFactory = connectionFactory;
        }
        public IEnumerable<T> QueryDataSet<T>(string sql, object param, CommandType commandType)
        {
            using (var connection = _connectionFactory.GetConnection())
            {
                connection.Open();

                var data = connection.Query<T>(sql, param, commandType: commandType);

                return data;
            }
        }

        public async Task<IEnumerable<T>> QueryDataSetAsync<T>(string sql, object param, CommandType commandType)
        {
            using (var connection = _connectionFactory.GetConnection())
            {
                connection.Open();

                var data = await connection.QueryAsync<T>(sql, param, commandType: commandType);

                return data;
            }
        }

        public T QueryFirst<T>(string sql, object param, CommandType commandType)
        {
            using (var connection = _connectionFactory.GetConnection())
            {
                connection.Open();

                var data = connection.QueryFirst<T>(sql, param, commandType: commandType);

                return data;
            }
        }

        public async Task<T> QueryFirstAsync<T>(string sql, object param, CommandType commandType)
        {
            using (var connection = _connectionFactory.GetConnection())
            {
                connection.Open();

                var data = await connection.QueryFirstAsync<T>(sql, param, commandType: commandType);

                return data;
            }
        }

        public T QueryFirstOrDefault<T>(string sql, object param, CommandType commandType)
        {
            using (var connection = _connectionFactory.GetConnection())
            {
                connection.Open();

                var data = connection.QueryFirstOrDefault<T>(sql, param, commandType: commandType);

                return data;
            }
        }

        public async Task<T> QueryFirstOrDefaultAsync<T>(string sql, object param, CommandType commandType)
        {
            using (var connection = _connectionFactory.GetConnection())
            {
                connection.Open();

                var data = await connection.QueryFirstOrDefaultAsync<T>(sql, param, commandType: commandType);

                return data;
            }
        }

        public T QuerySingle<T>(string sql, object param, CommandType commandType)
        {
            using (var connection = _connectionFactory.GetConnection())
            {
                connection.Open();

                var data = connection.QuerySingle<T>(sql, param, commandType: commandType);

                return data;
            }
        }

        public async Task<T> QuerySingleAsync<T>(string sql, object param, CommandType commandType)
        {
            using (var connection = _connectionFactory.GetConnection())
            {
                connection.Open();

                var data = await connection.QuerySingleAsync<T>(sql, param, commandType: commandType);

                return data;
            }
        }

        public T QuerySingleOrDefault<T>(string sql, object param, CommandType commandType)
        {
            using (var connection = _connectionFactory.GetConnection())
            {
                connection.Open();

                var data = connection.QuerySingleOrDefault<T>(sql, param, commandType: commandType);

                return data;
            }
        }

        public async Task<T> QuerySingleOrDefaultAsync<T>(string sql, object param, CommandType commandType)
        {
            using (var connection = _connectionFactory.GetConnection())
            {
                connection.Open();

                var data = await connection.QuerySingleOrDefaultAsync<T>(sql, param, commandType: commandType);

                return data;
            }
        }

        public async Task<IEnumerable<TParent>> QueryParentChildAsync<TParent, TChild, TParentKey>(string sql, Dictionary<TParentKey, TParent> cache,
            Func<TParent, TChild, TParent> map, dynamic param = null, string splitOn = "Id", IDbTransaction transaction = null, bool buffered = true,
            int? commandTimeout = null, CommandType? commandType = null)
        {
            using (var connection = _connectionFactory.GetConnection())
            {
                connection.Open();
                await connection.QueryAsync(sql, map, param as object, transaction, buffered, splitOn, commandTimeout, commandType);
                return cache.Values.ToList();
            }
        }

        public async Task ExecuteAsync(string sql, object param, CommandType commandType)
        {
            using (var connection = _connectionFactory.GetConnection())
            {
                connection.Open();

                await connection.ExecuteAsync(sql, param, commandType: commandType);
            }
        }
    }
}
