﻿using Campaign.Domain.Entities;
using Campaign.Domain.Enums;
using Campaign.Domain.SharedKernel;
using System;
using System.Collections.Generic;
using System.Text;

namespace Campaign.Domain.Appgregates
{
    public class ApplicationAggregate : Entity
    {
        public string Name { get; private set; }
        public string AppKey { get; private set; }
        public DeviceType DeviceType { get; private set; }
        public List<ApplicationCampaign> ApplicationCampaigns { get; private set; }

        public ApplicationAggregate(string name, string appKey, DeviceType deviceType)
        {
            Name = name;
            AppKey = appKey;
            DeviceType = deviceType;
        }
    }
}
