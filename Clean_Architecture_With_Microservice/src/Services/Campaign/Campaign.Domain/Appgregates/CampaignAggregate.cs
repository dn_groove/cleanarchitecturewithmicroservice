﻿using Campaign.Domain.Entities;
using Campaign.Domain.Enums;
using Campaign.Domain.SharedKernel;
using System;
using System.Collections.Generic;
using System.Text;

namespace Campaign.Domain.Appgregates
{
    public class CampaignAggregate : Entity
    {
        public string Name { get; private set; }
        public int Budget { get; private set; }
        public int UsedBudget { get; private set; }
        public DeviceType DeviceType { get; private set; }
        public List<ApplicationCampaign> ApplicationCampaigns { get; private set; }

        public CampaignAggregate(string name, int budget, int usedBudget, DeviceType deviceType)
        {
            Name = name;
            Budget = budget;
            UsedBudget = usedBudget;
            DeviceType = deviceType;
        }
    }
}
