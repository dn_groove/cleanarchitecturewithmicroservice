﻿using Campaign.Domain.SharedKernel;
using System;
using System.Collections.Generic;
using System.Text;

namespace Campaign.Domain.Appgregates
{
    public class UserAggregate : Entity
    {
        public Guid Id { get; private set; }
        public string Name { get; private set; }
        public string PasswordHash { get; private set; }
        public string Email { get; private set; }

        public UserAggregate(string name, string passwordHash, string email)
        {
            Name = name;
            PasswordHash = passwordHash;
            Email = email;
        }
    }
}
