﻿using Campaign.Domain.Enums;
using Campaign.Domain.SharedKernel;
using System;
using System.Collections.Generic;
using System.Text;

namespace Campaign.Domain.Entities
{
    public class Application : Entity
    {
        public string Name { get; set; }
        public string AppKey { get; set; }
        public DeviceType DeviceType { get; set; }
    }
}
