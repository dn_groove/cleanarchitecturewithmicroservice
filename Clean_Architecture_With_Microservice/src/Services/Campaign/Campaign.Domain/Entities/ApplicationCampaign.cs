﻿using Campaign.Domain.Appgregates;
using System;
using System.Collections.Generic;
using System.Text;

namespace Campaign.Domain.Entities
{
    public class ApplicationCampaign
    {
        public int CampaignId { get; set; }
        public int ApplicationId { get; set; }
        public CampaignAggregate Campaign { get; set; }
        public ApplicationAggregate Application { get; set; }
    }
}
