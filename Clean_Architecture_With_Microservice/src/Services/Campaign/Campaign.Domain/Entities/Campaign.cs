﻿using Campaign.Domain.Enums;
using Campaign.Domain.SharedKernel;
using System;
using System.Collections.Generic;
using System.Text;

namespace Campaign.Domain.Entities
{
    public class Campaign : Entity
    {
        public string Name { get; set; }
        public int Budget { get; set; }
        public int UsedBudget { get; set; }
        public DeviceType DeviceType { get; set; }
    }
}
