﻿using MediatR;
using System;
using System.Collections.Generic;
using System.Text;

namespace Campaign.Domain.SharedKernel
{
    public class Entity
    {
        public int Id { get; private set; }

        private List<INotification> _domainEvents;
        public IReadOnlyCollection<INotification> DomainEvents => _domainEvents?.AsReadOnly();

        public void AddDomainEvent(INotification eventItem)
        {
            _domainEvents = _domainEvents ?? new List<INotification>();
            _domainEvents.Add(eventItem);
        }
        public void ClearDomainEvents()
        {
            _domainEvents?.Clear();
        }
    }
}
