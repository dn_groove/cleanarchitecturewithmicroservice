﻿
using Campaign.Domain.Appgregates;
using Campaign.Domain.Entities;
using Campaign.Domain.SharedKernel;
using MediatR;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Storage;
using System;
using System.Collections.Generic;
using System.Data;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

namespace Campaign.EntityFramework
{
    public class ApplicationDBContext : DbContext, IUnitOfWork
    {
        public DbSet<UserAggregate> Users { get; set; }
        public DbSet<CampaignAggregate> Campaigns { get; set; }
        public DbSet<ApplicationAggregate> Applications { get; set; }

        private readonly IMediator _mediator;
        public ApplicationDBContext(DbContextOptions options, IMediator mediator) : base(options)
        {
            _mediator = mediator ?? throw new ArgumentNullException(nameof(mediator));
        }


        public async Task<bool> SaveEntitiesAsync(CancellationToken cancellationToken = default(CancellationToken))
        {
            await _mediator.DispatchDomainEventsAsync(this);
            var result = await base.SaveChangesAsync();
            return true;
        }

        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {
            modelBuilder.Entity<ApplicationCampaign>()
                .HasKey(ac => new { ac.CampaignId, ac.ApplicationId });
            modelBuilder.Entity<ApplicationCampaign>()
                .HasOne(ac => ac.Campaign)
                .WithMany(c => c.ApplicationCampaigns)
                .HasForeignKey(bc => bc.CampaignId);
            modelBuilder.Entity<ApplicationCampaign>()
                .HasOne(ac => ac.Application)
                .WithMany(a => a.ApplicationCampaigns)
                .HasForeignKey(ac => ac.ApplicationId);
        }
    }
}
