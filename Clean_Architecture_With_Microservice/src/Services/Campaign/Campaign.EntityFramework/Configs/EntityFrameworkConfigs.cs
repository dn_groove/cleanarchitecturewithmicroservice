﻿using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using System;
using System.Collections.Generic;
using System.Text;

namespace Campaign.EntityFramework.Configs
{
    public static class EntityFrameworkConfigs
    {
        public static IServiceCollection AddEntityFrameworkConfigs(this IServiceCollection services, IConfiguration configuration)
        {
            services.AddDbContext<ApplicationDBContext>(
                options => options.UseSqlServer(configuration.GetConnectionString("CommandDatabase")));

            return services;
        }
    }
}
