﻿using Request.Domain.SharedKernel;
using System;
using System.Collections.Generic;
using System.Text;

namespace Request.Domain.Entities
{
    public class DismissRequest : Entity
    {
        public string AppKey { get; set; }
        public string DeviceId { get; set; }
        public string IpAddress { get; set; }
        public string CampaignId { get; set; }
        public string DismissEvent { get; set; }
    }
}
