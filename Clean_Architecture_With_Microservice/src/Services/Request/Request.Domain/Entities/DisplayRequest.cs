﻿using Request.Domain.SharedKernel;
using System;
using System.Collections.Generic;
using System.Text;

namespace Request.Domain.Entities
{
    public class DisplayRequest : Entity
    {
        public string AppKey { get; set; }
        public string DeviceId { get; set; }
        public string IpAddress { get; set; }
        public string CampaignId { get; set; }
        public string DisplayEvent { get; set; }
    }
}
